var mymap = L.map('main_map').setView([4.638746489093824, -74.10473734260667], 13);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZXN0ZWJpYXJ0IiwiYSI6ImNrdm1wOXk1dDNoMHoyb3AxYnVwcDgwOGIifQ.Hd8h3vKGmKAlgGqSfAeWug'
}).addTo(mymap);



var marker = L.marker([4.6280948348489765, -74.11078951749829]).addTo(mymap);

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})